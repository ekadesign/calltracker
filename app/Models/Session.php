<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    use HasFactory;

    protected $table = 'ct_call_track_sessions';

    protected $fillable = ['id', 'json', 'is_finished'];
}
