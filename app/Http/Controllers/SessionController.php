<?php

namespace App\Http\Controllers;

use App\Models\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class SessionController extends Controller
{
    public function init(Request $request)
    {

        $id = Str::uuid();
        $session = Session::create(['id' => $id]);
        return response()->json([
            'success' => true,
            'data' => $session
        ]);
    }

    public function store($id, Request $request)
    {
        if(!$session = Session::find($id)){
            throw new \Exception('can\'t find a session');
        }

        $session->update(['json' => $request->get('json'), 'is_finished' => $request->get('is_finished')]);

        return response()->json([
            'success' => true,
            'data' => $session
        ]);
    }
}
